\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

In realtime rendering on GPU, we want to achieve a correct result no matter the order in which objects in a 3D scene are rendered. Hence, we render a new pixel only if it is closer to the camera than the pixel already present on the screen. This way, only the objects closest to the camera will be visible in the final image.
For this a depth buffer\cite{RealTimeRendering4DepthBuffer} is used - a 2D texture in size of the window we are rendering into in which a distance from the camera for each pixel on the screen is stored.

If we try to render a new pixel onto a screen we first have to compare it's distance from the camera to the distance stored inside the depth buffer. Only if the pixel is closer than the already stored value we render it and update the distance in the depth buffer. This process is called depth testing. Thanks to the depth testing, we can render objects in a scene in any particular order. This gives us an option to for example render objects in such an order that expensive state changes in the GPU will be minimized, which increases the performance of rendering\cite{RealTimeRendering4}.

However, when rendering a transparent surface we want to be able to see the objects occluded by it as well. Therefore, the color of each transparent pixel has to be blended with its background.

Modern GPU hardware supports blending of transparent surfaces, yet, the resulting image happens to be dependant on the rendering order of the transparent objects, because we can always keep only the last rendered pixel. During blending of a transparent pixel, the color of the covered pixel must already be pre-computed. If we render an object closer to the camera before an overlapping object that is further away, then all sorts of visual artifacts will be present in the final image as can be seen in figure \ref{fig:HardwareBlending}.

Enabling depth write during transparent object rendering would cause all farther transparent objects not to be visible. On the other hand, not writing to the depth buffer may cause those objects to overlap with foreground, producing incorrect results. As a result, it is necessary to render transparent objects from back to front.

\begin{figure}[H]
\caption{An example of image rendered with randomized ordering of transparent objects(left). Notice the green smoke in the background covering multiple balls that are much closer to the camera. Image rendered with correct ordering(right)}
\label{fig:HardwareBlending}
\includegraphics[width=70mm]{img/hardware_unsorted.png}
\includegraphics[width=70mm]{img/hardware_sorted.png}
\end{figure}

Rendering transparent objects from back to front might not be always straightforward, as multiple transparent objects might overlap or the entire 3D scene might be composed of only one complex object.

We might then decide to sort individual triangles which the objects are made of and render those in the correct order. Not only would this approach be incredibly computationally inefficient (objects might be composed of thousands or even millions of triangles), but the issue would still remain present as 2 or more triangles might overlap.

Rendering of a transparent geometry is hence not a simple task. Over the years, multiple approaches to rendering without a need of sorting have been invented, many of which have found use in commercial software\cite{AlanWake}\cite{CodOIT}.

This class of rendering algorithms is called Order Independent Transparency (OIT)\cite{OitCoined}.

As a part of the thesis, we created a C++ program capable of rendering semi-transparent scenes with multiple OIT algorithms.
\newline
\newline
The rest of the thesis is organized as follows: chapter \ref{Chapter1} contains the state of the art of OIT methods focusing more on the ones we've picked up for closer examination and comparison. Chapter \ref{ChapterOitAlgo} describes detailed implementation of the algorithms we chose, followed by a chapter \ref{ChapterResults} where we discuss the results.