\chapter{\label{ChapterOitAlgo}Implemented Transparency Rendering Algorithms in Detail}
We decided to implement and investigate the following OIT algorithms in detail. We picked all Blended OIT methods (\textit{WBOIT}, \textit{weighted average}, \textit{weighted sum}, \textit{moment transparency}) and we also implemented the \textit{depth peeling} algorithm for the ground truth(sorted like) comparison.

In this chapter we intent to describe both the implementation and theoretical details of the chosen algorithms and also the implementation of the application capable of rendering the scene using these algorithms.

For rendering one frame of a scene, the currently used algorithm receives
\begin{itemize}
    \item a list of opaque and transparent objects. Each object is represented as a set of vertices in 3D space, indices, which connect individual vertices into triangles and a texture.
    \item Position and a rotation of the camera in the scene for the current frame and its perspective matrix. Using these two, a world-to-view matrix can be calculated, which can transform positions of individual vertices in a world space onto a screen space.
    \item A set of stable data that persist between the frames. These can be compiled GPU programs used for rendering, additional framebuffers or other type of data that is required by the algorithm but does not necessary have to change between frames.
\end{itemize}

It would be possible to use one algorithm for rendering of the solid background, as each of the algorithms has to do this step, and then using a second algorithm to only add the transparent objects to the result. We decided not to use this approach and instead let each of the algorithms render opaque objects by itself, as this solution gives us more flexibility in implementation of individual algorithms.

\algrenewcommand{\algorithmiccomment}[1]{\textit{\textcolor{gray}{// #1}}}

\section{Depth Peeling}
\textit{Depth peeling} algorithm produces an accurate result for a cost of higher run time. In practice, it must render transparent geometry in multiple passes, collecting ("peeling") some layers of transparent geometry away and storing them in an accumulation buffer.

The algorithm uses two depth buffers to find transparent geometry closest to the camera which was not yet rendered. In each pass we collect only the geometry closest to the front buffer. Hence, the number of passes is proportional to number of transparent layers present.

\xxx{TODO: insert demonstration image}

\xxx{TODO: insert image that explains how it works}

\textit{Depth peeling} algorithm has seen a further improvement since 2001 in a form of \textit{dual depth peeling}\cite{DualDepthPeelingAndWeightedAverage} and \textit{multi layer depth peeling}\cite{MultiLayerDepthPeeling}, which introduce a way to peel more than one layer of transparent surfaces per render pass.

Our implementation "peels" only one layer of transparent surfaces each pass and runs in predetermined number of iterations. This is the basic version of the algorithm as stated in the source paper \cite{DepthPeelingOriginal} and it does not use any optimizations from later proposals \cite{MultiLayerDepthPeeling}\cite{DualDepthPeelingAndWeightedAverage}.
\newline
\newline
The \textit{depth peeling} algorithm can be implemented the following way\ref{alg:DepthPeeling}
\begin{algorithm}[H]
\caption{Depth Peeling pseudocode}
\label{alg:DepthPeeling}
\begin{flushleft}
\textbf{Require: }Framebuffers $F_1=(C_1, D_1)$, $F_2=(C_2, D_2)$ and $F_3=(C_3, D_3)$ with \textit{RGBA8} color components $C_1, C_2, C_3$ and depth buffers $D_1, D_2, D_3$
\newline
\textbf{Output: }Transparent scene rendered in $F_1$ framebuffer

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\Statex \Comment{Cleanup the framebuffers after the previous frame}
\State $C_3$ \gets $(0, 0, 0, 1)$, $D_3$ \gets 0
\State $C_1$ \gets background color, $D_1$ \gets 1
\State Render all solid geometry and depth into $C_1$ and $D_1$
\For{number of iterations}
    \Statex \Comment{Copy the depth of the opaque objects}
    \State $D_2$ \gets $D_1$
    \Statex \Comment{The closest layer of geometry that is further from camera than $D_3$ and closer to the camera then $D_2$ will be rendered}
    \State Render all transparent geometry into $C_2$ and $D_2$ without blending as if it was solid. Read and write depth using $D_2$ with hardware support. Simultaneously use $D_3$ as a texture and discard all pixels that are closer to the camera than distance in $D_3$
    \Statex \Comment{Now, we need to blend the closest layer into an accumulator}
    \State Enable blending
    \State Set separate blending operator for RGB channels as $C_f = C_s \cdot \alpha_d + C_d \cdot 1$
    \State Set separate blending operator for alpha channel as $C_f = C_s \cdot 0 + C_d \cdot (1-\alpha_s)$
    \State Do a full screen pass and blend $C_2$ into $C_3$
    \State Disable blending
    \Statex \Comment{Save the distance to the closest layer of transparent geometry so that we do not render it again in next iteration}
    \State $D_3$ \gets $D_2$ (might be omitted in the last iteration)
\EndFor
\Statex \Comment{Merge the color of transparent objects from accumulator with the opaque background}
\State Enable blending and set blending operator to $C_f = C_s \cdot 1 + C_d \cdot \alpha_s$
\State Do a full screen pass and blend accumulated color in $C_3$ into $C_1$
\end{algorithmic}
\end{algorithm}

\section{Weighted Sum}

\xxx{TODO: Maybe show the whole blending equation and color out the order independant members?}

\xxx{TODO: New images for Meshkin}

The \textit{weighted sum} algorithm calculates the final color $C_f$ in the following way
\begin{equation}
    C_f = \sum_{i=1}^{n}C_i \alpha_i + C_0 \left(1 - \sum_{i=1}^{n} \alpha_i \right)
\end{equation}

The algorithm requires one solid pass, one transparent pass and one full screen pass at platforms where blending of negative values is not possible.

Notice that the right part of the equation might produce values that are higher than 1 or negative. In OpenGL, clamping of values into an interval $[0, 1]$ or $[-1, 1]$ occurs\cite{OpenGLBlending} for 8bit unsigned and signed colors respectively. However, the floating point colors of accuracy \textit{F16} or \textit{F32} are not clamped. Using these, it would be possible to implement \textit{weighted sum} algorithm using only one framebuffer with color accuracy $F16$.

Meshkin algorithms produces plausible results for objects with low opacity and ground truth results if there is only one layer of transparent objects . However, in scenes with higher alpha values the sum of the alphas and colors produces large values that are displayed in the final image as oversaturated towards the color of transparent pixels.

\begin{figure}
\caption{Smoke particles in Sponza scene. \textit{Depth peeling}(left) and \textit{weighted sum}(right)}
\includegraphics[width=60mm]{img/meshkin_comparison_dp.png}
\includegraphics[width=60mm]{img/meshkin_comparison_meshkin.png}
\end{figure}

An implementation of \textit{weighted sum} might look like this. This implementation does not use the optimization with only one framebuffer of $F16$ type mentioned above.
\begin{algorithm}[H]
\caption{Possible implementation of weighted sum}
\begin{flushleft}
\textbf{Require: }Framebuffers $F_1 = (C_1, D_1)$, $F_2 = (C_2, D_2)$ and $F_3 = (C_3, D_3)$. $F_1$ and $F_3$ have \textit{RGBA8} colors, $F_2$ has \textit{RGBA16F} color
\newline
\textbf{Output: }Transparent scene rendered in $F_3$ framebuffer

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\Statex \Comment{Cleanup after the previous frame}
\State $C_1$ \gets background color
\State $D_1$ \gets 1
\State Render all opaque geometry into $C_1$ and $D_1$
\Statex \Comment{Cleanup the framebuffer into which we sum up the colors}
\State $C_2$ \gets $(0, 0, 0, 0)$
\State $D_2$ \gets $D_1$
\State \Comment{We want to turn depth writing so that we render all layers of transparent geometry, not just the closest one}
\State Turn off depth writing, but still use $D_2$ for depth testing
\State Enable blending with blend operator $C_f = C_s \cdot 1 + C_d \cdot 1$
\State Render all transparent geometry into $C_2$
\State Turn off blending
\Statex \Comment{Blending of negative values does not work for RGBA8 color buffers, so instead of blending we simply compute the final color in the shader}
\State Do a full screen pass into $C_3$. Use $C_1$ and $C_2$ as textures and calculate the final color $C_f = C_2.rgb + C_1.rgb \cdot (1 - C_2.a)$
\end{algorithmic}
\end{algorithm}

\section{Weighted Average}
At GDC 2008 Bavoid and Myers\cite{DualDepthPeelingAndWeightedAverage} presented (together with \textit{dual depth peeling} mentioned above) an OIT method of their own. Their \textit{weighted average} algorithm computes the resulting color of transparent scene in a following manner

\begin{equation}
    C_f = \frac{\sum_{i=1}^{n}C_i \cdot \alpha_i}{\sum_{i=1}^{n} \alpha_i} \left(1-\left[ \frac{1}{n} \sum_{i=1}^{n}\alpha_i \right]^n \right) + C_0 \cdot \left[ \frac{1}{n} \sum_{i=1}^{n}\alpha_i \right]^n
\end{equation}

\textit{Weighted average} renders the transparent geometry in 2 passes and uses 2 render targets for the first pass. In the first pass it accumulates the color values and the alphas of transparent layers into one render target and uses the second render target as an integral counter for the number of transparent layers per pixel.

The second pass is a full screen pass during which the accumulated colors and alphas are averaged and blended with the solid background rendered previously.
We effectively sum up colors and alphas of all consecutive transparent pixels and therefore a higher precision color buffer type is required (one that is not clamped, so usually $F16$ or $F32$).

The ideal condition for this algorithm is a fully transparent scene with mid to low range alpha values. In such a case the \textit{weighted average} produces images that are believable to a human eye, yet not always quite realistic.

\xxx{Show some images of WS in ideal conditions}

The averaging of consecutive surfaces might cause visible artefacts in the final image if the transparent objects of multiple colors are present (semi-transparent bushes in Sponza scene for example). The closest transparent pixels have the same importance in the final result as the ones behind them covered by multiple layers of transparent geometry.

\begin{figure}[H]
\caption{Overbleeding of transparent plants from behind the smoke particles in \textit{weighted average} algorithm(left). \textit{Depth peeling}(right)}
\includegraphics[width=70mm]{img/ws_comparison_ws.png}
\includegraphics[width=70mm]{img/ws_comparison_dp.png}
\end{figure}

\begin{figure}[H]
\caption{Sponza scene with decreased transparency. Color of the leaves at the front gets averaged with background in \textit{weighted average}(left). \textit{Depth peeling}(right)}
\includegraphics[width=70mm]{img/ws_comparison2_ws.png}
\includegraphics[width=70mm]{img/ws_comparison2_dp.png}
\end{figure}

In an environment where all alpha values $\alpha_i$ are the same the background coverage is computed exactly. However, this might not apply for scenes with varying alpha values.

Other issue is that the number $n$ might be composed of objects with transparency $\alpha_i = 0$. This might cause artifacts in scenes where the opacity of objects varies.
\begin{figure}[H]
\caption{(Sponza scene with low resolution leaves textures)In \textit{weighted average}(left), parts of the leaves geometry are fully transparent, however, these will still count into the number of consecutive surfaces and will introduce slight artifacts. \textit{Depth peeling}(right)}
\includegraphics[width=70mm]{img/ws_comparison3_ws.png}
\includegraphics[width=70mm]{img/ws_comparison3_dp.png}
\end{figure}

We implemented \textit{weighted average} method using framebuffer components of both 32bit floating colors (\textit{R32F} and \textit{RGBA32F}) and 16bit colors (\textit{R16F} and \textit{RGBA16F}). Instead of attaching a depth buffer we used a render buffer as we do not need to read the depth during the runtime of the algorithm.
\newline
\newline
For \textit{weighted sum}, we will describe the calculations of some shaders it uses, because these are not as trivial as in the previous algorithms.

We will use shader $S_{oit}: A \times N \rightarrow C$, where $A$ is the accumulated \textit{RGBA} color, $N$ is a floating point number greater than 0 and $C$ is the resulting \textit{RGBA} color. The shader calculates the resulting color in the following way.
\begin{equation}
    S_{oit}(A, N) = (\frac{A.rgb}{A.a}, \left[\frac{A.a}{N}\right]^N)
\end{equation}
It will be used for blending of accumulated colors into the final image.

We will also need a shader $S_{accum}: C \rightarrow A \times N$, which receives a color of the pixel $C$ and outputs \textit{RGBA} color $A$ and a float value $N$ in a following way
\begin{equation}
    S_{accum}(C) = (C.rgb \cdot C.a, C.a) \times (1)
\end{equation}
This one will be used for accumulating the color values into multiple render targets.

\begin{algorithm}[H]
\caption{Weighted Average pseudo code}
\begin{flushleft}
\textbf{Require: }2 framebuffers $F_1 = (C_1, D_1)$ and $F_2 = (C_{2s}, C_{2q}, D_2)$, where $D_1$ and $D_2$ are render buffers and $C_1$ is color component of type $RGBA8$. $C_{2s}$  and $C_{2a}$ are color components of types either $R32F$ and $RGBA32F$ or $R16F$ and $RGBA16F$
\newline
\textbf{Output: }Transparent scene rendered in $F_1$ framebuffer

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\State $C_1$ \gets background color
\State $D_1$ \gets 1
\State Render all solid geometry into $C_1$ and $D_1$
\State $C_{2q}$ \gets $(0, 0, 0, 0)$
\State $C_{2s}$ \gets $0$
\Statex \Comment{Use the solid depth so that we only render transparent geometry that is not occluded by the solid one}
\State $D_2$ \gets $D_1$
\State Enable blending with blend operator $C_f = C_d \cdot 1 + C_s \cdot 1$
\State Turn off depth writing
\Statex \Comment{Using blending, we store the number of consecutive surfaces into $C_{2s}$ and accumulate their colors into $C_{2q}$ for each pixel on the screen}
\State Render all transparent geometry into $C_2$. During rendering, write 1 into $C_{2s}$ for each rendered pixel and write it's color and alpha into $C_{2q}$. We do this using an $S_{accum}$ shader
\State Change blending operator to $C_f = C_s \cdot (1-\alpha_s) + C_d \cdot \alpha_s$
\State Do a full screen pass into $F_1$ during which we sample values from $C_{2s}$ and $C_{2q}$ and blend them with color which is already in $C_1$ using the $S_{oit}(C_{2q}, C_{2s})$ shader
\end{algorithmic}
\end{algorithm}


\section{WBOIT}
In 2013 McGuire and Bavoil introduced new blending algorithm\cite{WBOIT} in their paper \textit{weighted blended order independent transparency} (we use \textit{WBOIT} for brevity).
The \textit{WBOIT} calculates the following color
\begin{equation}
C_f = \frac{\sum_{i=1}^{n}C_i \cdot \alpha_i \cdot w(z_i, \alpha_i)}{\sum_{i=1}^{n}a_i \cdot w(z_i, \alpha_i)} \left(1-\prod_{i=1}^{n}(1-\alpha_i)\right) + C_0 \cdot \prod_{i=1}^{n}(1-\alpha_i)
\end{equation}
We should note that this equation differs from the one stated in the original paper by the member $\alpha_i$ on the left side in numerator. Bavoil and McGuire assumed an alpha-premultiplied color in their paper, which we do not and therefore we explicitly state the $\alpha_i$ member in the equation.

During the transparent geometry pass the colors and alphas of all transparent objects are summed up into two render targets. Later, during the full screen pass these colors are averaged out and blended with solid background. Notice the $w(z_i, \alpha_i)$ weight function, which assigns weights to individual pixels of transparent geometry based on their distance and alpha. This way, the objects closer to the camera should have greater weight in the average than the objects further from it. This improves upon the problem of averaging multiple surfaces which \textit{weighted average} algorithm has.

\xxx{TODO: show how WBOIT improves over WA}

To compute a coverage of the background \textit{WBOIT} uses a clever trick during the blending stage, thanks to which the individual alpha channels (1 - $\alpha_i$) of the transparent objects are multiplied (instead of summed up). Note that this product $\prod_{i=1}^{n}(1-\alpha_i)$ is the actual coverage of the background. Unlike the previous method, this one computes the correct coverage in all circumstances.

One possible implementation of the weight function is to use none at all, e.g. $w(z_i, \alpha_i) = 1$, which simply averages all transparent objects and blends them with solid background. This version of the algorithm inherits some weaknesses of the \textit{weighted average} algorithm, but calculates the coverage of solid background more accurately than it's predecessor.

\xxx{TODO: show difference between some weight function and no weight function}

Notice that for $w(z_i, \alpha_i) = 1$ and $a_i = a_j$ for all pairs $i$ and $j$ the \textit{WBOIT} equation produces the same result as \textit{weighted average} equation. This means that the \textit{WBOIT} algorithm without a weight function calculates the same color as \textit{weighted average} in environments where all alpha values of transparent objects are the same.

As the source paper states, one disadvantage of the weight functions is that simply rescaling all scene geometry around the camera moves it into different depth range (distance from all objects changes) and will result in different image.

\xxx{TODO: show how rescaling a scene changes it's transparency}

Our implementation of \textit{WBOIT} uses an accumulation shader $S_{accum}: C \times Z \rightarrow A \times T$, where $C$ is an \textit{RGBA} color, $Z$ is a depth of a pixel in $[0, 1]$ interval and $T$ and $A$ are two render targets with float and \textit{RGBA} types. The shader calculates output color in the following way
\begin{equation}
    S_{accum}(C, Z) = \left(C.rgb \cdot C.a \cdot W(Z, C.a), C.a \cdot W(Z, C.a)) \times (C.a\right)
\end{equation}
This shader requires a weight function $W(Z, A)$, which assigns an importance to individual samples based on their alpha value $A$ and distance from camera $Z$.

We implemented the following weight functions
\begin{equation*}
\begin{aligned}
    & W_1(Z, \alpha) = 1 && W_2(Z, \alpha) = \frac{1}{Z^3} &&  W_3(Z, \alpha) = \frac{1}{Z^5} \\
    &  W_4(Z, \alpha) = \frac{1}{Z^3} \cdot \alpha \quad &&  W_5(Z, \alpha) =\frac{1}{Z^5} \cdot \alpha
\end{aligned}
\end{equation*}
Each of the weight function can be made \textit{adaptive} by dividing the distance $Z$ by $Z_s$ before feeding it into the $W$ function. Here, $Z_s$ is the distance to the closest opaque surface (we can read it from the depth buffer). No transparent surface will be further from the camera than $Z_s$ and therefore $Z/Z_s \in [0, 1]$.
\newline
\newline
Another shader we need is a merging shader $S_{oit}: A \times T \rightarrow C$, where $A$ and $T$ are accumulated \textit{RGBA} colors and multiplied alphas. It renders color in the following way
\begin{equation}
    S_{oit}(A, T) = \left(\frac{A.rgb}{A.a}, T\right)
\end{equation}

\begin{algorithm}[H]
\caption{WBOIT Algorithm pseudocode}
\begin{flushleft}
\textbf{Require: }Framebuffers $F_1=(C_1, D_1)$ and $F_2=(C_{2s}, C_{2q}, D_2)$. $C_1$ is of type $RGBA8$, $C_{2s}$ is of type $R32F$  and $C_{2q}$ has type $RGBA32F$
\newline
\textbf{Output: }Transparent scene rendered in $F_1$

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\Statex \Comment{Cleanup after the previous frame}
\State $C_1$ \gets background color
\State $D_1$ \gets 1
\State Render all solid geometry into $C_1$ and it's depth into $D_1$ with depth testing and writing on and blending off
\State $D_2$ \gets $D_1$
\Statex \Comment{This value is the background coverage. We start with 1 and then we lower it by multiplying it with (1-$\alpha$) of each transparent surface}
\State $C_{2s}$ \gets 1
\Statex \Comment{These is the color accumulator. We start with no colors and then add some during the rendering}
\State $C_{2q}$ \gets $(0, 0, 0, 0)$
\State Disable depth writing, but keep depth testing on
\State Use 2 render targets $C_{2s}$ and $C_{2q}$
\State Enable blending
\State Set the blending operator of $C_{2s}$ to $C_f = C_s \cdot 0 + C_d \cdot (1-\alpha_s)$
\State Set the blending operator of $C_{2q}$ to $C_f = C_s \cdot 1 + C_d \cdot 1$
\State Render all transparent geometry into $C_{2s}$ and $C_{2q}$. The final color is calculated as $C_{2q}, C_{2s} = S_{accum}(C)$, where $C$ is an \textit{RGBA} color of the currently rendered transparent pixel
\State Use blend operator $C_f = C_s \cdot (1-\alpha_s) + C_d \cdot \alpha_s$
\State Do a full screen pass into $F_1$. $C_1$ already contained color of opaque geometry and in this step we only add transparent objects
\end{algorithmic}
\end{algorithm}


\section{Moment Transparency}
The last OIT approach we implemented is the moment transparency. For sake of brevity we will use name \textit{"MBOIT"}.
\textit{MBOIT} is quite similar to its predecessor \textit{WBOIT} described above. The main difference is the modified weight function
\begin{equation}
w(z, \alpha) = exp(-Hamburger4MSM(\frac{d}{\hat{d}}, z))
\end{equation}
the rest of the equation remains the same
\begin{equation}
C_f = \frac{\sum_{i=1}^{n}C_i \cdot \alpha_i \cdot w(z_i, \alpha_i)}{\sum_{i=1}^{n}a_i \cdot w(z_i, \alpha_i)} \left(1-\prod_{i=1}^{n}(1-\alpha_i)\right) + C_0 \cdot \prod_{i=1}^{n}(1-\alpha_i)
\end{equation}

Instead of leaving a decision to choose a weight function upon a programmer, \textit{MBOIT} calculates it by itself in one extra render pass. During this additional render pass moments of transparent surfaces are collected and summed up. In later render pass these moments are retrieved and used to calculate the appropriate weights.

As the source paper states, the new weight function calculates the coverage of transparent pixels correctly only for 2 or less surfaces covered by each other. If there are more than 2 surfaces overlapping then the resulting coverage is only approximated.

\xxx{TODO: Show MBOIT in ideal case, compare with WBOIT}

The source papers suggest multiple versions of this algorithm. Either by using more moment passes or using trigenometric or quantitized moments instead of power moments.

We implemented the basic version of \textit{MBOIT} algorithm with one transparency render pass for collecting power moments and one render pass for accumulating the transparency.

A description of the \textit{Hamburger4MSM} algorithm from the \textit{moment shadow mapping} paper\cite{MSM} follows. We will need this later in the $S_{weight}$ shader.

\begin{algorithm}[H]
\caption{Hamburger4MSM Algorithm}
\begin{flushleft}
\textbf{Input: } $b \in \rm I\!R^4$, fragment depth $z_f \in \rm I\!R$, bias $\alpha$
\newline
\textbf{Output: } Coverage amount

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\State $b' \coloneqq (1-\alpha) \cdot b + \alpha \cdot (0.5, 0.5, 0.5, 0.5)^T$
\State Use a Cholesky decomposition to solve for $c \in \rm I\!R^3$:
\Statex $\begin{pmatrix}
1 & b'_1 & b'_2 \\
b'_1 & b'_2 & b'_3 \\
b'_2 & b'_3 & b'_4
\end{pmatrix} \cdot c = \begin{pmatrix}
1 \\
z_f \\
z_f^2
\end{pmatrix}$
\State Solve $c_3 \cdot z^2 + c_2 \cdot z + c_1 = 0$ for $z$ using quadratic formula and let $z_2, z_3 \in \rm I\!R$ denote the solutions
\State If $z_f \leq z_2$: Return $0$
\State Else if $z_f \leq z_3$: Return $\frac{z_f \cdot z_3-b'_1 \cdot (z_f + z_3) + b'_2}{(z_3-z_2) \cdot (z_f - z_2)}$
\State Else: Return $1-\frac{z_2 \cdot z_3-b'_1 \cdot (z_2 + z_3) + b'_2}{(z_f-z_2) \cdot (z_f - z_3)}$
\end{algorithmic}
\end{algorithm}

We will use additional shader $S_{moment}: Z \times \alpha \rightarrow M \times S$, which receives depth $Z$ of the pixel, its transparency $\alpha$ and outputs 4 moments in $M$ in \textit{RGBA} form and their sum $S$ as a float.
\begin{equation}
    S_{moment}(z, \alpha) = ((z, z^2, z^3, z^4) \cdot -log(1-\alpha))  \times (-log(1-\alpha))
\end{equation}

Later, we use the shader $S_{weight}: M \times S \times Z \rightarrow T$, which receives previously calculated moments $M$ in \textit{RGBA} form, their sum $S$ as a float and the depth of a pixel $Z$. This shader approximates the coverage of a pixel in depth $Z$.
\begin{equation}
    S_{weight}(M, S, Z) = exp(-Hamburger4MSM(M/S, Z, 0) \cdot S)
\end{equation}

\begin{algorithm}[H]
\caption{MBOIT Algorithm pseudocode}
\label{alg:Mboit}
\begin{flushleft}
\textbf{Require: }The same resources as \textit{WBOIT} algorithm with addition of framebuffer $F_M = (C_{Ms}, C_{Mq}, D_M)$ where $C_{Ms}$ and $C_{Mq}$ have types \textit{R32F} and \textit{RGBA32F} respectively
\newline
\textbf{Output: }Transparent scene rendered in $F_1$

\hrulefill
\end{flushleft}
\begin{algorithmic}[1]
\Statex \Comment{We are essentially doing the same algorithm, just with one additional render pass and different weight function}
\State Do steps 1-7 of the \textit{WBOIT} algorithm
\State $D_M$ \gets $D_1$
\State $C_{Ms}$ \gets $0$
\State $C_{Mq}$ \gets $(0, 0, 0, 0)$
\State Enable blending and set blending operator to $C_f = C_s \cdot 1 + C_d \cdot 1$
\Statex \Comment{Capture moments of transparent geometry for each pixel on the screen}
\State Render all transparent geometry into $F_M$ and capture moments $z, z^2, z^3, z^4$ into $C_{Mq}$ and their sum into $C_{Ms}$ as $C_{Mq}, C_{Ms} = S_{moment}(Z)$ where $Z$ is the depth of the rendered transparent pixel
\State Do steps 8-11 of the \textit{WBOIT} algorithm
\State Do step 12 of \textit{WBOIT}, but calculate the weight function by $S_{weight}$ shader instead of $W(z, \alpha)$. The resulting weight is $S_{weight}(C_{Mq}, C_{Ms}, Z)$, where $Z$ is the depth of the currently rendered transparent pixel
\State Do rest of the steps just like \textit{WBOIT}
\end{algorithmic}
\end{algorithm}