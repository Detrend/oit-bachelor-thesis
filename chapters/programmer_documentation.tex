\chapter{Developer Documentation}
In this section we will explain the structure of our code, point out the most interesting files and how is the whole codebase structured.

This section assumes basic knowledge of programming in C++ and understanding of how realtime rendering works.

\subsubsection{How To Compile And Run}
The code was compiled using C++20 version of the language and MSVC compiler.

To compile and run use the attached Visual Studio 2022 Solution \texttt{/OIT.sln}.

\subsubsection{Dependencies and Libraries}
For development of the demo app we used the following C++ libraries

\begin{itemize}
    \item \texttt{SDL} for creation of the window and OpenGL context
    \item \texttt{OpenGL} rendering API for rendering on GPU
    \item \texttt{GLAD} for OpenGL function bindings
    \item \texttt{ImGui} for creating a simple user interface
    \item \texttt{Nlohmann Json} for parsing JSON files used for scenes
    \item \texttt{STB image} for loading \textit{.png} files used as textures
    \item \texttt{GLM} for 3D linear algebra
    \item \texttt{Assimp} for loading 3D models in various file formats
    \item \texttt{Tiny File Dialogs} for cross platform implementations of open/save file popups
\end{itemize}

Source code for all of the above mentioned libraries is included in the \texttt{/dependencies} directory. No additional dependencies have to be installed in order to compile the project on Windows using Visual Studio.

\subsubsection{Most Notable Files}
All header files are located in directory \texttt{/source/include/} and their implementation can be found inside \texttt{/source/source/}.

List of the most interesting files follows. All files are relative to the directory \texttt{/source/}.
\begin{itemize}
    \item \texttt{/source/main.cpp} contains the entry point of the program
    \item \texttt{/source/demo\_app.cpp} and \texttt{/include/demo\_app.h} contain source code of the demo application
    \item \texttt{/include/common.h} contains definitions for macros and types used across the whole program
    \item \texttt{/include/camera.h} and \texttt{/source/camera.cpp} contain functionality for the 3D world camera
    \item \texttt{/include/shaders.h} and \texttt{/source/shaders.cpp} contain helper functions for compiling and linking GPU programs used for rendering
    \item \texttt{/include/geometry.h} and \texttt{/source/geometry.cpp} contain helper functions for loading of 3D scenes from files
    \item \texttt{/include/render\_object.h} and \texttt{/source/render\_object.cpp} contain functionality for representing the objects inside the world
\end{itemize}

\subsubsection{Implementation Of Transparency Rendering Algorithms}
Source code for individual algorithms is located inside \texttt{/Source/\-Include/\-Algorithms/} and \texttt{/Source/\-Source/\-Algorithms/}. Each of the implemented algorithms is located inside of it's own file.
\newline
\newline
\texttt{/Source/Include/Algorithms/} and \texttt{/Source/Source/Algorithms/} contain the following files:
\begin{itemize}
    \item \texttt{common.h} and \texttt{common.cpp} contain the implementations of functionality shared across all algorithms
    \item \texttt{depth\_peeling.h} and \texttt{depth\_peeling.cpp} implement the \textit{depth peeling} algorithm
    \item  \texttt{meshkin.h} and \texttt{meshkin.cpp} contain implementation of \textit{weighted sum} algorithm
    \item \texttt{moment.h} and \texttt{moment.cpp} contain implementation of the \textit{MBOIT} algorithm
    \item \texttt{solid\_only.h} and \texttt{solid\_only.cpp} do not contain any transparency rendering algorithm. Instead, functions inside these files are used for rendering of the opaque only geometry in the scene
    \item \texttt{wboit.h} and \texttt{wboit.cpp} contain implementation of \textit{WBOIT} algorithm
    \item and \texttt{weighted\_average.h} with \texttt{weighted\_average.cpp} contain implementation of the \textit{weighted average} algorithm
\end{itemize}
All algorithms are wrapped inside the namespace \texttt{oit::algorithms} and each one has an individual namespace reserved for itself. For example, the whole functionality of \textit{depth peeling} algorithm can be found inside the namespace \texttt{oit::algorithms::depth\_peeling}.
\newline
\newline
Even though each algorithm works in a slightly different manner, they all share a similar interface. Each of our algorithms implements the following functions, which are then called from the main app in a certain circumstances.
\begin{itemize}
    \item \texttt{init} is called during the intialization of the application. This function should initialize all data required for the runtime of the algorithm and return them to the application
    \item \texttt{terminate} is a function that should cleanup all resources of the given algorithm and prepare for application shutdown. It is called just before the application turns off
    \item \texttt{on\_screen\_resize} is called if the size of the application window changes. Each algorithm should resize it's framebuffers (if it uses any) to match the size of the main window
    \item \texttt{render\_world} is called when the given algorithm is requested to render one frame of a scene
    \item \texttt{draw\_interface} is called during drawing of \textit{ImGUI} interface. This function allows each algorithm to define a set of adjustable parameters that can be visible inside the UI
\end{itemize}

\subsubsection{Scene Representation}
For rendering, we use a structure of data named \textit{render object} which resides inside \texttt{/source/include/\-render\_object.h} and \texttt{/source/source/\-render\_\-object.cpp}.
It consists of OpenGL handles for vertex buffer, index buffer and texture and size, position and rotation.
\newline
\newline
Since each \textit{render object} can have only one texture, for rendering of a scene with multiple textures we have \textit{render scenes}, which are each a set of \textit{render objects} with additional position, rotation and scaling. The rendered world then consists of multiple \textit{render scenes}.

We group objects into \textit{render scenes} so that positioning a part of a scene or removing it is not difficult for the user. For instance, if the user wants to remove a particle cloud they created before then it is not necessary to remove each particle individually. Instead, only the \textit{render scene} which contains all the particles is removed.

\textit{Render scenes} are defined inside the same files as \textit{render objects}.

\subsubsection{Shaders}
Shaders written in the GLSL language are located inside the \texttt{/shaders/} folder.

Each of the transparency rendering algorithms defines which shaders it uses inside of its header file.

For instance, the \textit{depth peeling} algorithm requires following shaders (\texttt{/source/\-include/\-depth\_peeling.h}):
\begin{lstlisting}[caption=Example of shaders used by algorithm]
constexpr cstr SOLID_SHADERS[] = {"shaders/solid.vert", "shaders/solid.frag"};
constexpr cstr PEEL_SHADERS [] = {"shaders/solid.vert", "shaders/peel.frag"};
constexpr cstr MERGE_SHADERS[] = {"shaders/full_screen_quad.vert", "shaders/peel_merge.frag"};
\end{lstlisting}

A helper functionality for compiling and linking shaders can be found inside the \texttt{/source/include/shaders.h} and \texttt{/source/source/shaders.h}.

\subsubsection{Serialization}
Some premade scenes are serialized to \textit{.json} format in directory \texttt{/scenes/}.

(De)serialized scenes have the following \textit{json} structure. The comments following after "//" are not a valid JSON and serve only as a description of the format.
\begin{lstlisting}[caption=Deserialized scene example]
{
  "back": [
    0.002739322604611516, // color of the background (R, G, B), normalized 
    0.019094884395599365, // in interval [0, 1]
    0.5588235259056091
  ],
  // list of objects in the scene
  "objects": [
    {
      // color of the object
      "color_override": [
        0.0,
        1.0,
        0.0,
        1.0
      ],
      "file_path": "models/sponza/Sponza.fbx", // source file
      // position of the object
      "position": [
        0.0,
        0.0,
        0.0
      ],
      // rotation of the object
      "rotation": [
        0.0,
        -0.0,
        0.0
      ],
      // scale of the object
      "scale": 0.009999999776482582,
      // mesh is an object loaded from file
      "type": "mesh"
    },
    {
      "color_override": [
        0.9177013039588928,
        0.9411764740943909,
        0.14302192628383636,
        1.0
      ],
      // number of particles
      "count": 16,
      "position": [
        0.0,
        0.0,
        0.0
      ],
      "rotation": [
        0.0,
        -0.0,
        0.0
      ],
      "scale": 1.0,
      // particle is a type of an object that is not loaded,
      // but instead generated
      "type": "particle"
    }
  ]
}
\end{lstlisting}
