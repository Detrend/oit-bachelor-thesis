\chapter{\label{ChapterResults}Results}

In this chapter, we discuss both the visual results and performance of implemented algorithms. We decided to compare all algorithms across multiple categories of rating to show their weaknesses and strengths in several situations. We will compare the results of all algorithms to the \textit{depth peeling} algorithm with 32 passes (or sometimes 48 passes in cases of a complex scene), because it produces the same result as if all surfaces were rendered from back to front.

\section{Final color}
Let's discuss first the accuracy of the final color calculation and and how it differs from the result we could have achieved if individual triangles of the scene were rendered from back to front (or rendered by \textit{depth peeling}).
\newline
\newline
We compared the visual look of algorithms across 2 different scenes with slight variations (for instance, placing smoke clouds around the scene). The scenes used were
\begin{itemize}
    \item \textit{sponza scene} - a famous architectural building used for testing of rendering algorithms
    \item \textit{ball park} - our own scene composed of multiple balls with varying colors and opacities
\end{itemize}
We should note that we rendered leaves in the \textit{sponza scene} as if they were partially transparent object, which is not that common in practical cases where the vegetation is drawn by different techniques. We did this to show artifacts produced by the algorithms if several contrasting transparent objects would be occluded by each other. If the vegetation was rendered properly, the problem would still remain present for objects like pieces of glass for example.
\newline
\newline
The most notable weakness of the \textit{weighted sum} algorithm is the overflowing of bright colors. This happens mostly in environments where multiple transparent surfaces are covered with each other as can be seen in figure\ref{fig:WeightedSumImg1}. The resulting image is oversaturated or sometimes even white as the \textit{weighted average} simply sums up the colors of covered surfaces multiplied with their alphas instead of properly blending them using alpha values. For surfaces with high alpha values this sum often exceeds the upper bound of the type format the color is stored in, which usually leads to the color being clamped as white.

\begin{figure}[H]
\caption{If multiple transparent surfaces overlap each other, the \textit{weighted sum}(left) algorithm produces bright and sometimes even white colors. Sorted-like image result achieved using \textit{depth peeling}(right).}
\includegraphics[width=70mm]{img/ws_comparison_6_ws.png}
\includegraphics[width=70mm]{img/ws_comparison_6_dp.png}
\newline
\includegraphics[width=70mm]{img/ws_comparison_5_ws.png}
\includegraphics[width=70mm]{img/ws_comparison_5_dp.png}
\label{fig:WeightedSumImg1}
\end{figure}

A certain divergence from the final color can be also spotted in images produced by the \textit{weighted average} algorithm. It is not so apparent in cases when transparent surfaces have low alpha, because the average is weighted towards pixels with greater opacity. This still produces a small error, however, it is not usually that apparent as can be seen in figure \ref{fig:WAGood}.

\begin{figure}[H]
\caption{Sponza scene with two low opacity smoke clouds rendered by \textit{weighted average}(left) and \textit{depth peeling}(right). The difference is not very notable.}
\includegraphics[width=70mm]{img/wa_dp_comp_wa.png}
\includegraphics[width=70mm]{img/wa_dp_comp_dp.png}
\label{fig:WAGood}
\end{figure}

However, due to averaging of alpha and color values of all overlapped surfaces instead of blending them from back to front the algorithm produces incorrect results in cases where multiple surfaces with similar alphas cover each other as can be observed in figure \ref{fig:WeightedAverageImg1}. This is most notable in form of overbleeding artifacts which we discuss more in the section \ref{OverbleedingSection}.

\begin{figure}[H]
\caption{The color of the background red smoke is clearly visible even though it should be fully occluded by the gray smoke in foreground. This is a common case of overbleeding. \textit{Weighted average} on left, \textit{depth peeling} with 32 passes on right}
\includegraphics[width=70mm]{img/wa_wboit_dp_comp1_wa.png}
\includegraphics[width=70mm]{img/wa_wboit_dp_comp1_dp.png}
\label{fig:WeightedAverageImg1}
\end{figure}

The averaging of transparent surfaces happens to be a problem for the \textit{WBOIT} algorithm as well. Nevertheless, it is not so visible in most cases as \textit{WBOIT} tries to combat this by utilizing a weight function which assigns more importance to closer surfaces. A notable improvement from \textit{weighted average} can be observed in figure \ref{fig:WboitImg1}.

In ideal conditions this completely solves the problem, although the weight function might in some cases overestimate or underestimate the coverage, resulting in an image that is slightly off. Most notable case of this is when the weight function assigns too much weight to close surface, resulting in poor visibility of background surfaces as can be seen in figure \ref{fig:WboitMboitDpComp}.

\begin{figure}[H]
\caption{\textit{WBOIT} gives much more weight on the smoke in foreground, yet, the result is still not perfect and overbleeding of vegetation can be spotted(left). \textit{Depth peeling}(right)}
\includegraphics[width=70mm]{img/wa_wboit_dp_comp1_wboit.png}
\includegraphics[width=70mm]{img/wa_wboit_dp_comp1_dp.png}
\label{fig:WboitImg1}
\end{figure}

Even though the weight function is quite helpful, it is sometimes difficult to control. Tweaking it to work well for a certain environments might cause it to underperform in different conditions as can be seen in figures \ref{fig:Wboit2weights} and \ref{fig:WboitSlightBlueSmoke}. It might also take an non-trivial time to find a fitting weight function for a given environment.

\begin{figure}[H]
\caption{In the right side of the image, both the hanging vines and leaves in the pot are not properly occluded by the transparent blue smoke. This is due to them getting assigned a small importance from the used weight function ($W=1/z^5 \cdot \alpha$) - \textit{WBOIT}(left). \textit{Depth peeling} (right).}
\includegraphics[width=70mm]{img/wboit_comp1_wboit.png}
\includegraphics[width=70mm]{img/wboit_comp1_dp.png}
\label{fig:WboitSlightBlueSmoke}
\end{figure}

The weight function might in some cases underperform, but overall it is better having a slightly underperforming weight function that having none at all.

\begin{figure}[H]
\caption{WBOIT with 2 different weight functions, each one producing different results. $W=1/z^3 \cdot \alpha$ on the left, $W=1/z^5 \cdot \alpha$ on the right. Note the visible difference in the almost completely transparent white ball on the right side of the images.}
\includegraphics[width=70mm]{img/wboit_comp2_z3a.png}
\includegraphics[width=70mm]{img/wboit_comp2_z5a.png}
\label{fig:Wboit2weights}
\end{figure}

\textit{MBOIT} solves the problem of carefully choosing a weight function for a certain scene by computing it itself. This results in a very believable images in a lots of situations as can be seen in figure \ref{fig:MboitImg1}. On average its results are closest to the \textit{depth peeling} as can be seen in figure \ref{fig:WboitMboitDpComp}.

Having said that, the \textit{MBOIT} has it's limits as well. It estimates the weight function correctly only in cases where 2 or less transparent surfaces overlap each other (as stated in the source paper\cite{MOMENT1}). For greater number of surfaces the weight function is only approximated and might sometimes perform even worse than \textit{WBOIT} as can be seen in figure \ref{fig:MboitWorseThanWboit}.

\begin{figure}[H]
\caption{Ball Park scene. Notice how WBOIT(left) (with $W=1/z^3 \cdot \alpha$) almost completely covers the balls behind the red one on the left. MBOIT(middle) computes coverage more accurately and covered balls have almost the same visibility as in case of depth peeling(right)}
\includegraphics[width=46mm]{img/mboit_wboit_dp_comp1_wboit.png}
\includegraphics[width=46mm]{img/mboit_wboit_dp_comp1_mboit.png}
\includegraphics[width=46mm]{img/mboit_wboit_dp_comp1_dp.png}
\label{fig:WboitMboitDpComp}
\end{figure}

\begin{figure}[H]
\caption{\textit{MBOIT}(left) produces pretty convincing result even in environments with multiple transparent objects. Note the imperfection in form of vegetation overbleeding to the foreground from behind the yellow smoke. \textit{Depth peeling}(right)}
\includegraphics[width=70mm]{img/mb_comp1_mb.png}
\includegraphics[width=70mm]{img/mb_comp1_dp.png}
\label{fig:MboitImg1}
\end{figure}

\begin{figure}[H]
\caption{Sponza scene with smokes of multiple colors. A slight overbleeding of background vegetation can be seen. MBOIT on left, WBOIT with a weight function $1/z^5 \cdot \alpha$ on the right}
\includegraphics[width=70mm]{img/mb_comp2_mb.png}
\includegraphics[width=70mm]{img/mb_comp2_wboit.png}
\label{fig:MboitWorseThanWboit}
\end{figure}

In it's best with weight function precisely chosen for a given scene, the \textit{WBOIT} will outperform \textit{MBOIT}. In spite of that, the \textit{MBOIT} will usually come on top in a common scene with varying alpha values, colors and distances between objects without a need to do any preparation or precalculation of it's weight function.

\section{\label{OverbleedingSection}Overbleeding and Image Stability}
Overbleeding is a rendering artifact produced by some of the transparency algorithms. It is caused by blending the transparent foreground differently with transparent background then the solid background.

\xxx{TODO: give visual example of overbleeding}

Since overbleeding is a frequent occurrence in all of the blended OIT algorithms and is usually the source of the most notable rendering artifacts we decided to dedicate an entire subsection to it.
\newline
\newline
By image stability we mean how much the resulting image changes for individual methods if we move the camera around or rescale the objects in the scene.

We decided to include this metric in our results because an algorithm might perform very well in a very specific conditions, but moving the camera or objects around might reveal significant rendering artifacts. This case might be the most notable in video games where the player might move around quickly.
\newline
\newline
Overbleeding usually comes hand in hand with image instability, therefore we decided to discuss these two phenomenon together in one subsection.
\newline
\newline
Both \textit{weighted sum} and \textit{weighted average} algorithms are not very resistant against overbleeding since they do not allocate more importance to surfaces closer to the camera. This might cause very visible overbleeding artifacts.

\xxx{TODO: Show image of overbleeding of WS and WA}

On the other hand, a lack of a weight function also results in almost 100\% image stability. No matter the distance from the object, it's scale or rendering order, the result remains the same.

\xxx{TODO: Image of WS and WA if we rescale the scene or move around it}
\newline
\newline
\textit{WBOIT} does not suffer that much from overbleeding as previous 2 algorithms. Thanks to the weight function, it is able to produce an image without any overbleeding in ideal conditions. However, conditions might not always be ideal.

One problem of the \textit{WBOIT} algorithm is its instability with moving around the scene or simply rescaling it. The weight function is usually quite sensitive to changing distances between objects or their scale.

\xxx{TODO: image of WBOIT instability if the scale or distance changes. Caption: moving just a few meters back from the smoke reveals overbleading}

For this reason we experimented with \textit{adaptive distance} in some of our weight functions. In certain scenarios, this improves the quality of the resulting image.

\xxx{TODO: show image where adaptive Z works well. Caption: adaptive Z improves the algorithm in short distance}

On the other hand, adaptive distance causes problems in other scenarios. Most notably in low alpha environments.

\xxx{TODO: show image where adaptive distance does not work well}

\textit{MBOIT} algorithm outperforms \textit{WBOIT} in most cases with both stability and a lack of overbleeding.

\xxx{TODO: show image comparing WBOIT and MBOIT}

Both of these problems are still present, but in much less cases.

\xxx{TODO: show images where overbleeding and instability are still present}

Our statement from the previous subsection applies here as well. For a given scene, there always exists a perfect weight function using which the WBOIT will outperform the \textit{MBOIT}.

\xxx{TODO: show image of WBOIT outperforming MBOIT}

However, searching for such a function might be difficult or straight up impossible in dynamic scenes where the environment changes from frame to frame (moving particle systems for example).

\section{Performance}
Performance of individual algorithms is important for real time rendering. In certain cases, we might prefer algorithm with inferior image quality but a better runtime performance.
For this reason, we also discuss how each algorithm performs in certain scenarios.
\newline
\newline
For benchmarking, we picked 3 different scenes
\begin{itemize}
    \item \textit{transparent sponza}: a fully transparent sponza scene. No objects in this scene are opaque
    \item \textit{sponza with smokes}: default opaque sponza scene with multiple transparent smoke clouds
    \item \textit{ball park}: scene with several balls of different colors and alphas and one particle cloud
\end{itemize}
\begin{figure}[H]
    \includegraphics[width=46mm]{img/benchmark_sponza_transparent.png}
    \includegraphics[width=46mm]{img/benchmark_sponza.png}
    \includegraphics[width=46mm]{img/benchmark_ballpark.png}
    \caption{Benchmarked scenes. Transparent sponza, sponza with smokes and ballpark from left to right}
\end{figure}
We also measured performance for rendering the given scene as fully solid and opaque with no transparency and rendering it with \textit{depth peeling} algorithm, both for comparison reasons.
\newline
\newline
All benchmarks were executed on Acer Nitro AN515-54 laptop with 16GB RAM, Intel Core i5-9300H CPU @ 2.4GHz and NVIDIA RTX 2060 GPU with Windows 10 operating system.
\\
\begin{table}
\caption{Benchmarked Performance of Transparency Rendering Algorithms}
\label{tab:PerfBenchmarks}
\begin{tabular}{|p{3.5cm}||p{4cm}|p{3cm}|p{2cm}|}
 \hline
 \multicolumn{4}{|c|}{Performance Measure} \\
 \hline
 & \multicolumn{3}{|c|}{Scene} \\
 \hline
 OIT Algorithm& Sponza Transparent&Sponza Smokes&Ball Park\\
 \hline
 Everything Opaque& 1.07ms&1.05ms&   0.51ms\\
 Weighted Sum&   1.85ms& 1.22ms&0.65ms\\
 Weighted Avg.&3.03ms& 1.53ms&  0.83ms\\
 WBOIT 16F&3.49ms& 1.56ms&  0.90ms\\
 WBOIT 32F&9.34ms& 2.6ms&  1.75ms\\
 MBOIT&   18.18ms& 4.21ms&3.25ms\\
 Depth Peeling (32)& 31.25ms&14.92ms&   9.43ms\\
 \hline
\end{tabular}
\end{table}

From the benchmarks \ref{tab:PerfBenchmarks} we can infer that the \textit{weighted sum} algorithm outperforms all other, in some cases by even 4x speedup. It renders the scene somewhat slower compared to rendering it fully opaque, which makes sense because the geometry for the whole scene has to be rendered and blended instead of only rendering the pixels closest to the camera.
\newline
\newline
Notice \textit{WBOIT 16F} and \textit{WBOIT 32F}, which are 2 versions of the \textit{WBOIT} algorithm. The first one uses no weight function and is therefore able to use 16bit floating point color buffer instead of the 32bit one.
Runtimes of both \textit{weighted average} and \textit{WBOIT 16F} closely match each other as their number of rendering passes is essentially the same and both of them use the same number of framebuffers and have color components of the same size.
\newline
\newline
\textit{MBOIT} algorithm introduces a significant drop in the performance due to increased amount of render passes required and more complex calculations. We should note that the source papers\cite{MOMENT1}\cite{MOMENT2} are aware of the performance drops and offer solutions in the form of decreased resolution or quantized moments. These optimizations increases the performance of the algorithm, but on the other hand decreases the image quality. We have not experimented with any of these optimizations.

\section{Memory Consumption}
We decided to compare memory footprint of all implemented algorithms as can be seen in table \ref{tab:MemoryBench}. We only state the amount of consumed GPU video memory as the RAM memory consumption is small and might depend on the software architecture of the rendering program.

We state the amount of memory our implementations of the algorithms consume. It might be possible to implement each algorithm using smaller amounts of memory. We simply did not attempt to optimize for the smallest possible memory consumption as the amount of memory on video cards is usually quite large compared to the amount required by individual implementations.
\newline
\newline
We should also note that the default framebuffer of a window usually consumes 24-32bits per pixel (depending on the presence of the alpha channel). Our numbers include the size of the default framebuffer as well (we assume the size of default framebuffer to be 32bits per pixel).
\begin{table}
\caption{Memory Consumption of Individual Transparency Algorithms}
\label{tab:MemoryBench}
\begin{tabular}{|p{5cm}||p{3cm}|p{2cm}|}
 \hline
 \multicolumn{3}{|c|}{Memory Consumption} \\
 \hline
 Algorithm& Per 1920x1080&Per Pixel\\
 \hline
 Depth peeling& 49,766MB&24B\\
 Weighted sum&   33,177MB& 16B\\
 Weighted avg. 16F&37,322MB& 18B\\
 WBOIT 16F&37,322MB& 18B\\
 WBOIT 32F&58,060MB& 28B\\
 MBOIT 32F&   99,532MB& 48B\\
 \hline
\end{tabular}
\end{table}

Notice that for certain algorithms in table we also stated the precision of floating point numbers we used - \textit{16F} for 16bit floating point precision and \textit{32F} for 32bit colors.
\newline
\newline
The \textit{weighted average} algorithm uses floating point color component. We experimented both with using both 16bit and 32bit floating point colors and observed no difference in the result.

Converting \textit{MBOIT} to lower precision format is not so straightforward, because the number precision might cause rounding of numbers close to 0 down, resulting in possible division by 0. Source papers for \textit{MBOIT}\cite{MOMENT1}\cite{MOMENT2} offer certain solutions to decreasing a memory footprint, which are however out of bounds of this thesis and were not implemented.