\chapter{\label{Chapter1}Transparency Rendering}
Here, we describe a several approaches used for rendering transparent geometry in order independent manner. But first, we are going to take a detour and explain how blending of transparent surfaces works.

\section {Blending}

\subsubsection{Blending Equation}
The blend equation introduced by Alvy Ray Smith and Ed Catmull in 1970\cite{AlphaChannel} dictates the color of the final pixel in a following manner
\begin{equation}
C_f = C_s \cdot \alpha_s + (1-\alpha_s) \cdot C_d
\label{blending_equation}
\end{equation}
where $C_f=(R_f, G_f, B_f)$ denotes the red, green and blue components of the final color and $\alpha_s$ denotes a transparency of the given pixel. $C_s$ and $C_d$ are colors of the transparent pixel and covered pixel. All color components are in range $[0, 1]$.

\subsubsection{Blending of Consecutive Surfaces}
If multiple transparent objects are covered by each other then the equation \ref{blending_equation} recursively unfolds into
\begin{equation}
C_f = C_1 \cdot \alpha_1 + (1-\alpha_1) \cdot (C_2 \cdot \alpha_2 + (1-\alpha_2) \cdot ... \cdot (C_n \cdot \alpha_n + (1-\alpha_n) \cdot C_0)
\label{multi_blend_equation}
\end{equation}
where $C_1 .. C_n$ and $\alpha_1 ..\alpha_n$ denote colors and alphas of $n$ transparent consecutive pixels and $C_0$ is a color of solid background (with $\alpha_0 = 1$).

\subsubsection{Coverage}
The equation \ref{multi_blend_equation} can be unfolded into the following form
\begin{equation}
C_f = C_1 \cdot \alpha_1 + (1-\alpha_1) \cdot C_2 \cdot \alpha_2 + (1-\alpha_1)(1-\alpha_2) \cdot C_3 \cdot \alpha_3 + ... + (1-\alpha_1) \cdot ... \cdot (1-\alpha_n) \cdot C_0
\end{equation}
If we rewrite it, we get
\begin{equation}
C_f = \sum_{i=1}^{n}C_i \cdot \alpha_i \cdot T_i(\alpha_1, ..., \alpha_n)
\label{coverage_equation}
\end{equation}
where
\begin{equation}
T_i(\alpha_1, ..., \alpha_n) = \prod_{j < i}(1-\alpha_j)
\end{equation}
Function $T_i(\alpha_1, ..., \alpha_n)$ is a Coverage of surface $i$ and it is a product of reversed alphas of all surfaces closer to the viewport then surface $i$. Coverage is a measure of how visible the given transparent surface is on the screen.

Notice that coverage function is non-increasing and if we know it in advance for some $n$ surfaces then we can convert blending of these surfaces into a sum as shown in \ref{coverage_equation}.

Coverage of a solid background by transparent surfaces $(C_1, \alpha_1), ..., (C_n, \alpha_n)$ is simply $\prod_{i=1}^{n}(1-\alpha_i)$.

\section{Explanation of OpenGL Technical Terms}
This section explains technical terms required to understand the implementation of algorithms.
\newline
\newline
\textbf{Framebuffer} is a set of multiple textures of the same resolution onto which we render using GPU. It consists of a depth buffer and at least one color attachment. We denote all framebuffers as $F = (C_1, ..., C_n, D)$, where $C_1$ .. $C_n$ are different color attachments (each might be of a different format and might have different number of channels) and $D$ is the depth buffer.
\newline
\newline
\textbf{Color attachment} is a texture into which we render during render pass. It can have up to 4 color channels and each channel can have different bit precision. Common types of color attachments include\cite{OpenGLColorFormats}
\begin{itemize}
    \item $RGBA8$ - color attachment with 4 channels, each composed of 8 bits. All values range in $[0, 1]$
    \item $RGBA16F/RGBA32F$ - 4 channels, each is a signed unclamped floating point number with 16/32bit precision
    \item $R16F/R32F$ - one signed floating point channel with 16/32bit color precision
\end{itemize}
During one render pass the GPU can write \textbf{into only one} framebuffer, however, if the framebuffer contains multiple color attachments then we can render into all of them in parallel. This is called rendering into \textbf{multiple render targets}.
\newline
\newline
During one render pass, we can write into only one framebuffer, but can read from multiple textures (this includes color attachments of different framebuffers). For example, we can render our scene into a texture in one step and then later read from the given texture during another render pass. It is forbidden to read from texture which is bound to the framebuffer we are rendering into.
\newline
\newline
We are not able to read from a texture we are writing to at the same time, however, modern rendering APIs allow us to use \textbf{blending}. If blending is enabled, then the color currently in a texture is not overwritten by the new source color, but instead blended with it by a certain blend operation.

We show some blending operations that are possible. $C_f$ denotes the final color of the given pixel after the blending operation ends, $C_s$ denotes a current color of the pixel and $C_s$ is the color we are writing into the pixel during rendering. We can also use alpha of the source color $C_s.a$ and alpha of the destination color $C_d.a$.
These are some of the possible blending operations\cite{OpenGLBlending}
\begin{itemize}
    \item $C_f = C_d \cdot 1 + C_s$ \cdot 1
    \item $C_f = C_d \cdot 0 + C_s$ \cdot $C_s.a$
    \item $C_f = C_d \cdot (1-C_s.a)+ C_s \cdot C_s.a$
    \item $C_f = C_d \cdot (1-C_s.a)+ C_s \cdot (1-C_d.a)$
\end{itemize}
A separate blending operator can be configured for a render target, or even for a channel of texture. For example, we can have one blending operation for the $RGB$ channels of a color component and another for the $A$ channel.
\newline
\newline
\textbf{Render buffer} is a type of depth buffer that can be used if we do not intend to read from it manually. It performs faster than classical depth buffers in some cases.
\newline
\newline
During rendering, we can \textbf{disable depth testing}. If depth testing is disabled, then pixels of all objects will be rendered into the framebuffer no matter their distance from the screen. Also, no pixels will be written into the depth buffer.
It is possible to only \textbf{disable depth write}. That way, the depth testing will be still enabled and only the pixels closer to the camera then the content of the depth buffer will be written. However, the values in depth buffer will not be updated.
We can use this for rendering the solid background and keeping the depth buffer around. During transparent rendering we will not write to it, but we will compare all transparent pixels to the values stored to it and therefore no objects that are occluded will be rendered.
\newline
\newline
\textbf{Shaders} are a simple programs written in GLSL language that run on the GPU. OpenGL supports multiple types of shaders, but we will limit ourselves here only to \textbf{fragment shaders}. These allow us to decide how is the color of the rendered object calculated. We will denote all of our shaders as functions that output one or more color values.
An example of a shader might be $S: C \rightarrow Z \times A$, which receives some value $C$ (might be RGBA color, floating point value or other) and outputs values $Z$ and $A$ into first and second render target respectively.

\section {Overview of the current OIT methods}
Multiple approaches to solving a problem with rendering of a transparent geometry have been developed, like the alpha buffer used in Pixar Reyes\cite{Pixar} for instance. Nonetheless, we will limit ourselves only to the ones that can be used in realtime graphics, where the performance is crucial. Now, lets finally get to the OIT rendering methods and their description.

\subsubsection{Depth Peeling}
We will start with the \textit{depth peeling}\cite{DepthPeelingOriginal}, which is an OIT rendering approach proposed in 2001. It renders the transparent geometry in multiple iterations and produces sorted like results (as if the geometry has been rendered in a sorted order).

In each iteration, the algorithm renders all of transparent geometry and collects ("peels") the front most rendered layer of transparent pixels, which are then blended into a separate buffer. For this 2 depth buffers are used - one which stores the distance to the furthest peeled layer and the second one which denotes distance from closest solid surface. \textit{Depth peeling} renders all objects in range between these 2 depth buffers and then moves the first depth buffer forward before the next iteration.

In each pass, only the closest non-rendered layer of transparent pixels is rendered and therefore we achieve an ordering. The number of passes is proportional to the number of transparent layers and in each pass all transparent objects have to be rendered, which puts the upper complexity bound of the algorithm in $O(n^2)$ region, where $n$ is the number of transparent layers.

\textit{Depth peeling} produces sorted like result(if we run it for a sufficient number of passes) for a cost of more rendering passes and a lower performance.

It was further improved by \textit{dual depth peeling} algorithm \cite{DualDepthPeelingAndWeightedAverage}, which peels 2 layers of transparent surfaces in each iteration and \textit{multi layer depth peeling}\cite{MultiLayerDepthPeeling}, which can peel 4 layers per iteration.

We decided to implement this method ourselves to compare image results of other methods against it.

\subsubsection{Stochastic Transparency}
This technique\cite{StochasticTransparency} introduced in 2011 treats all transparent pixels as opaque and discards some of them in the fragment shader. The number of discarded pixels is proportional to transparency of a surface. Follow up technique, \textit{depth-based stochastic transparency}, introduced in the same paper takes more rendering passes and in turn produces more realistic image for a cost of a higher runtime. 

\textit{Stochastic transparency} and \textit{depth-based stochastic transparency} turn transparency problem into rendering of solid geometry.
Similar approach was used in the video game GTA5\cite{GTA5} for rendering of a distant vegetation.

It overall produces much more convincing results then \textit{weighted sum} technique and has better runtime than \textit{depth peeling}.

\subsubsection{Adaptive Transparency and Multi Layer Alpha Blending}
\textit{Adaptive transparency}\cite{AdaptiveTransparency} and \textit{multi layer alpha blending}\cite{MultiLayerAlphaBlending} are approaches which store fixed amount of samples per pixel (in the first case only the transparency and the depth of a fragment are stored, in second one we store also the color).

If the number of transparency layers exceeds the fixed amount then two samples are merged into one. The first approach uses 2 transparent geometry passes, collecting and merging the samples in the first one and then later approximating coverage function for individual transparent pixels in the second pass. The second technique requires only one transparency rendering pass.

These techniques produce a sorted-like result for a scenes with a small amount of transparent surfaces, yet require more memory than other approaches listed.

\subsubsection{Weighted Sum}
\textit{Weighted sum} introduced by Meshkin in 2007\cite{Meshkin} takes a rather different approach at rendering transparency. Instead of trying to achieve a ground truth result it strives for simplicity and only estimates the final image by summing up the non-order-dependent members of the blending equation \ref{multi_blend_equation}.

The produced results are rarely correct if multiple layers of transparent geometry are present and \textit{weighted sum} often produces brighter colors because colors of the transparent surfaces are summed instead of interpolated.

However, it makes up for this disadvantage by a great run time, as it requires only one transparent geometry pass and produces convenient results for a geometry with low transparency values. It is the first Blended OIT\cite{WBOIT} algorithm and it lay the ground for further improvements in the field over the years.

\subsubsection{Weighted Average}
\textit{Weighted average}\cite{DualDepthPeelingAndWeightedAverage}, which was introduced together with the \textit{dual depth peeling}\cite{DualDepthPeelingAndWeightedAverage} in 2008 is another Blended OIT algorithm and it builds upon \textit{weighted sum} in multiple ways.

It averages the color and alpha values of all transparent objects into a separate buffer. Then, in a full screen pass the coverage of background is approximated from the averaged alpha channel and the averaged transparent colors are blended with solid background.

Similar to \textit{weighted sum}, the resulting image is correct if there is only one layer of transparent surfaces. For any higher number of surfaces the algorithm does not produce a correct result.

Also, due to averaging colors of all transparent surfaces without putting any significance to their distance from viewpoint, it produces visible artifacts if objects with a low transparency are covered by a high-transparency geometry. This problem is further explored and improved by the \textit{weighted blended OIT}\cite{WBOIT} algorithm.

Nonetheless, it is particularly accurate at rendering transparent geometry with similar transparency across the whole scene and the final image looks much more convincing then the \textit{weighted sum}.

\subsubsection{Weighted Blended OIT - WBOIT}
\textit{Weighted Blended OIT}\cite{WBOIT} algorithm builds on the ideas of \textit{weighted average} approach mentioned above, but improves its blending functionality in multiple ways. The background coverage is calculated precisely in all cases by multiplying alphas of all consecutive transparent pixels. Similar to the \textit{weighted average}, it averages all transparent surfaces, but this time multiplies them with weights, assigning more importance to certain surfaces over others.

The weight function tries to approximate the coverage function \ref{coverage_equation}. The paper itself proposes multiple ways to calculate weights, often based upon transparency levels and distance of individual surfaces.

The resulting image looks very appealing and it is a significant improvement over \textit{weighted average} for similar runtime and memory consumption. It requires less memory or rendering passes then \textit{adaptive transparency} and \textit{depth-based stochastic transparency}, yet falls short in certain corner cases.

\subsubsection{Moment Transparency - MBOIT / MOMENT OIT}
\textit{Moment based OIT}\cite{MOMENT1} or \textit{moment OIT}\cite{MOMENT2}, introduced in 2018 by 2 papers independently, improves upon \textit{WBOIT} by introducing a new, more precise weights function.

The new weight function is computed during the runtime of the algorithm in a separate transparent geometry pass using \textit{HamburgerMSM} algorithm used in \textit{moment shadow mapping}\cite{MSM}. During this pass the optical depth of the transparent geometry with depth moments is stored into a separate framebuffer. In the next rendering pass the coverage function is approximated from the stored data and used as a weight function for individual surfaces.
This approach found its use in the game Alan Wake 2 released in 2023\cite{AlanWake}.

For brevity, we will use the name \textit{MBOIT} to denote both of these methods.